package helper

import "os"

// Getenv is
func Getenv(key string) (fallback string) {
	value := os.Getenv(key)
	if len(value) == 0 {
		if key == "GORM_CONNECTION" {
			fallback = "host=localhost port=5432 user=postgres dbname=efishery sslmode=disable password=hhjsdjERT768&%5^$#^&&^DRhsjgDHfwhg@#7V56353HfDmHfhFFFcBahdhT"
		} else if key == "JWT_SECRET" {
			fallback = "76ykjNJKHkjnkjdnehS$%^@#@mkmdskj"
		} else if key == "GORM_LOG" {
			fallback = "true"
		}
		return fallback
	}
	return value
}
