package model

import (
	"github.com/jinzhu/gorm"
)

type (

	// User is
	User struct {
		gorm.Model
		Phone    string `gorm:"type:varchar(15)" json:"phone"`
		Name     string `gorm:"type:varchar(225)" json:"name"`
		Role     string `gorm:"type:varchar(15)" json:"role"`
		Password string `gorm:"type:varchar(225)" json:"password"`
	}

	// Comudities is
	Comudities struct {
		gorm.Model
		UUID         string `gorm:"type:varchar(300)" json:"uuid"`
		Komoditas    string `gorm:"type:varchar(250)" json:"komoditas"`
		AreaProvinsi string `gorm:"type:varchar(250)" json:"area_provinsi"`
		AreaKota     string `gorm:"type:varchar(250)" json:"area_kota"`
		Size         string `gorm:"type:numeric" json:"size"`  // Type size dibuat string karena dari jsonnya bentuknya string harusnya bentuknya numeric atau float
		Price        string `gorm:"type:numeric" json:"price"` // Type price dibuat string karena dari jsonnya bentuknya string harusnya bentuknya numeric atau float
		TglParsed    string `gorm:"type:timestamp with time zone" json:"tgl_parsed"`
		Timestamp    string `gorm:"type:bigint" json:"timestamp"` // Type timestamp dibuat string jsonnya bentuknya string
	}

	// GroupCommudities is
	GroupCommudities struct {
		TglParsed     string  `json:"tgl_parsed"`
		AreaProvinsi  string  `json:"area_provinsi"`
		RataRataPrice string  `json:"rata_rata_price"`
		MaxPrice      float64 `json:"max_price"`
		MinPrice      float64 `json:"min_price"`
		MedianPrice   float64 `json:"median_price"`
		MaxSize       float64 `json:"max_size"`
		MinSize       float64 `json:"min_size"`
		MedianSize    float64 `json:"median_size"`
	}

	// Error is
	Error struct {
		Code    int         `json:"code"`
		Status  string      `json:"status"`
		Message interface{} `json:"message"`
	}
)

// TableName is
func (User) TableName() string {
	return "user"
}

// TableName is
func (Comudities) TableName() string {
	return "comudities"
}
