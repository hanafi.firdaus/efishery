# Backend Task

## Spesifikasi
1. Postgresql
2. Framework Echo Middleware
3. Redis
4. Gorm (ORM database golang)
5. Clean Architecture skeleton

## Petunjuk penggunaan
1. Install redis
2. export GO111MODULE=on (Untuk mengaktifkan go module)
3. Install PostgreSQL 12.3 (Ubuntu 12.3-1.pgdg18.04+1)
4. Gunakan golang versi go1.13.8
5. Untuk menjalankan project go menggunakan command ```go run .```
6. Jalankan backend scheduller update kurs USD, projectnya ada di (https://gitlab.com/MHFirdaus/efishery-scheduller)
7. Jalankan program
8. Jika ingin setting env ada di file .env
9. Table database akan otomatis ke generate, nampun harus di setting terlebih dahula databasenya.

## Setting database
1. Buat db dengan nama db efishery
2. Untuk username dan password bisa menyesuaikan atau mengikuti setting database di file .env
3. Execute Query SQL di postgresql editor
```
CREATE OR REPLACE FUNCTION _final_median(numeric[])
   RETURNS numeric AS
$$
   SELECT AVG(val)
   FROM (
     SELECT val
     FROM unnest($1) val
     ORDER BY 1
     LIMIT  2 - MOD(array_upper($1, 1), 2)
     OFFSET CEIL(array_upper($1, 1) / 2.0) - 1
   ) sub;
$$
LANGUAGE 'sql' IMMUTABLE;

CREATE AGGREGATE median(numeric) (
  SFUNC=array_append,
  STYPE=numeric[],
  FINALFUNC=_final_median,
  INITCOND='{}'
);
```