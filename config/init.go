package config

import (
	"github.com/go-redis/redis/v8"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"github.com/labstack/echo"
)

// Config is
type Config struct {
	redis *redis.Client
	echo  *echo.Echo
	gorm  *gorm.DB
}

// Init is
func Init() {
	var cfg Config

	err := godotenv.Load()
	if err != nil {
		panic(err)
	}

	err = cfg.Redis()
	if err != nil {
		panic(err)
	}

	err = cfg.Postgresql()
	if err != nil {
		panic(err)
	}

	err = cfg.Labstack()
	if err != nil {
		panic(err)
	}

	err = cfg.Services()
	if err != nil {
		panic(err)
	}

	cfg.echo.Logger.Fatal(cfg.echo.Start(":10000"))

}
