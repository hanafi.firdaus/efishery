package config

import (
	helper "efishery-api/helper"
	"efishery-api/model"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// Postgresql is
func (cfg *Config) Postgresql() (err error) {

	connectionString := helper.Getenv("GORM_CONNECTION")
	db, err := gorm.Open("postgres", connectionString)
	if err != nil || db.Error != nil {
		return
	}

	db.CreateTable(&model.User{})
	db.CreateTable(&model.GroupCommudities{})
	db.CreateTable(&model.Comudities{})
	cfg.gorm = db
	return
}
