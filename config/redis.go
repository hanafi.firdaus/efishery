package config

import (
	"context"

	"github.com/go-redis/redis/v8"
)

var ctx = context.Background()

// Redis is
func (config *Config) Redis() (err error) {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	config.redis = client

	_, err = config.redis.Ping(ctx).Result()
	if err != nil {
		return
	}
	return
}
