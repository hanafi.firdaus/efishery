package config

import (
	"efishery-api/service/delivery/routers"
	"efishery-api/service/repository/postgres"
	"efishery-api/service/usecase"
)

// Services is
func (cfg Config) Services() (err error) {

	UserRepo := postgres.CreateUserRepository(cfg.gorm)
	UserUsecase := usecase.CreateUsersUsecase(UserRepo)

	FetchRepo := postgres.CreateFetchRepository(cfg.gorm)
	FetchUsecase := usecase.CreateFetchUsecase(FetchRepo)

	routers.Routers(cfg.echo, cfg.redis, UserUsecase, FetchUsecase)
	return
}
