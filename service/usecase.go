package service

import (
	"efishery-api/model"
)

// UserUseCase is
type UserUseCase interface {
	CreateUser(model.User) (model.User, error)
	GetUser(phone string) (model.User, error)
}

// FetchUseCase is
type FetchUseCase interface {
	FetchDataComudities([]model.Comudities) error
	AddComidities(model.Comudities) error
	GetAggregateDataComudities(model.GroupCommudities) ([]model.GroupCommudities, error)
}
