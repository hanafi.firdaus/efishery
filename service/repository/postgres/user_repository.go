package postgres

import (
	"efishery-api/model"
	"fmt"

	"efishery-api/helper"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// UserRepository is
type UserRepository struct {
	db *gorm.DB
}

// CreateUserRepository is
func CreateUserRepository(db *gorm.DB) UserRepository {
	return UserRepository{db}
}

// CreateUserDB is
func (bio UserRepository) CreateUserDB(user model.User) (out model.User, err error) {

	pass := ""
	err = bio.db.Where("phone = ?", user.Phone).Find(&user).Error
	if err != nil {
		if err.Error() == "record not found" {

			// generate random password
			pass = helper.RandomWords(4)

			// hasing password
			hash, errGenerateHashPass := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
			if errGenerateHashPass != nil {
				return out, errGenerateHashPass
			}

			user.Password = string(hash)
			err = bio.db.Create(&user).Error
			if err != nil {
				return
			}
		}
	} else {
		err = fmt.Errorf("This phone nomor has been register")
	}

	out = user
	out.Password = pass
	return
}

// GetUser is
func (bio UserRepository) GetUser(phone string) (user model.User, err error) {
	err = bio.db.Where("phone = ?", phone).Find(&user).Error
	if err != nil {
		return
	}
	return
}
