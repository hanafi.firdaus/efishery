package postgres

import (
	"efishery-api/model"
	"fmt"
	"strings"

	"github.com/jinzhu/gorm"
)

// FetchRepository is
type FetchRepository struct {
	db *gorm.DB
}

// CreateFetchRepository is
func CreateFetchRepository(db *gorm.DB) FetchRepository {
	return FetchRepository{db}
}

// FetchDataComudities is
func (f FetchRepository) FetchDataComudities(com []model.Comudities) (err error) {

	for _, element := range com {
		if element.TglParsed != "" || element.Timestamp != "" {
			element.TglParsed = strings.Replace(element.TglParsed, "gmt+0700", "", -1)
			err = f.db.Create(&element).Error
			if err != nil {
				// Error diabaikan karena banyak error di bagian tglParsednya
			}
		}
	}

	return
}

// AddComidities is
func (f FetchRepository) AddComidities(com model.Comudities) (err error) {
	err = f.db.Create(&com).Error
	if err != nil {
		return
	}
	return
}

// GetAggregateDataComudities is
func (f FetchRepository) GetAggregateDataComudities(com model.GroupCommudities) (out []model.GroupCommudities, err error) {
	query := `select * from(
		select date_trunc('week', c.tgl_parsed) as tgl_parsed, c.area_provinsi, avg(c.price) as rata_rata_price,
		max(c.price) as max_price, min(c.price) as min_price, median(c.price) as median_price,
		max(c.size) as max_size, min(c.size) as min_size, median(c.size) as median_size
		from comudities c group by (c.tgl_parsed, c.area_provinsi)
	) as a where LOWER(a.area_provinsi) = LOWER('%s') and tgl_parsed = '%s'`
	err = f.db.Raw(fmt.Sprintf(query, com.AreaProvinsi, com.TglParsed)).Scan(&out).Error
	if err != nil {
		return
	}
	return
}
