package usecase

import (
	"efishery-api/model"
	service "efishery-api/service"
)

// UserUsecase is
type UserUsecase struct {
	UserRepository service.UserRepository
}

// CreateUsersUsecase is
func CreateUsersUsecase(UserUseCase service.UserRepository) service.UserUseCase {
	return UserUsecase{UserUseCase}
}

// CreateUser is
func (b UserUsecase) CreateUser(u model.User) (bio model.User, err error) {
	return b.UserRepository.CreateUserDB(u)
}

// GetUser is
func (b UserUsecase) GetUser(phone string) (model.User, error) {
	return b.UserRepository.GetUser(phone)
}
