package usecase

import (
	"efishery-api/model"
	service "efishery-api/service"
)

// FetchUsecase is
type FetchUsecase struct {
	FetchRepository service.FetchRepository
}

// CreateFetchUsecase is
func CreateFetchUsecase(FetchUseCase service.FetchRepository) service.FetchUseCase {
	return FetchUsecase{FetchUseCase}
}

// FetchDataComudities is
func (f FetchUsecase) FetchDataComudities(com []model.Comudities) error {
	return f.FetchRepository.FetchDataComudities(com)
}

// AddComidities is
func (f FetchUsecase) AddComidities(com model.Comudities) error {
	return f.FetchRepository.AddComidities(com)
}

// GetAggregateDataComudities is
func (f FetchUsecase) GetAggregateDataComudities(com model.GroupCommudities) ([]model.GroupCommudities, error) {
	return f.FetchRepository.GetAggregateDataComudities(com)
}
