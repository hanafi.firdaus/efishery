package service

import "efishery-api/model"

// UserRepository is
type UserRepository interface {
	CreateUserDB(model.User) (model.User, error)
	GetUser(phone string) (model.User, error)
}

// FetchRepository is
type FetchRepository interface {
	FetchDataComudities([]model.Comudities) error
	AddComidities(model.Comudities) error
	GetAggregateDataComudities(model.GroupCommudities) ([]model.GroupCommudities, error)
}
