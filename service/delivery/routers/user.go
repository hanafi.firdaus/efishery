package routers

import (
	helper "efishery-api/helper"
	"efishery-api/model"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
)

// CreateUser is
func (h Handler) CreateUser(c echo.Context) error {

	var param = new(model.User)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	bio, err := h.UserUseCase.CreateUser(*param)
	if err != nil {
		return helper.Errors(c, 400, err)
	}
	return c.JSON(200, bio)
}

// Login is
func (h Handler) Login(c echo.Context) error {

	var param = new(model.User)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	user, err := h.UserUseCase.GetUser(param.Phone)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(param.Password))
	if err != nil {
		return helper.Errors(c, 401, err)
	}

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = user.Name
	claims["phone"] = user.Phone
	claims["role"] = user.Role
	claims["timestamp"] = time.Now()

	tkn, err := token.SignedString([]byte(helper.Getenv("JWT_SECRET")))
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(200, map[string]string{
		"token": tkn,
	})
}

// Restricted is
func (h Handler) Restricted(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	return c.JSON(200, map[string]string{
		"name":      claims["name"].(string),
		"role":      claims["role"].(string),
		"phone":     claims["phone"].(string),
		"timestamp": claims["timestamp"].(string),
	})
}
