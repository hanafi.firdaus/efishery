package routers

import (
	helper "efishery-api/helper"
	"efishery-api/service"

	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// Handler is
type Handler struct {
	UserUseCase  service.UserUseCase
	FetchUseCase service.FetchUseCase
	Redis        *redis.Client
}

// Routers is
func Routers(e *echo.Echo, redis *redis.Client, usr service.UserUseCase, fetch service.FetchUseCase) {
	handler := Handler{
		UserUseCase:  usr,
		FetchUseCase: fetch,
		Redis:        redis,
	}
	e.POST("/api/v1/user/create", handler.CreateUser)
	e.POST("/api/v1/user/token", handler.Login)

	r := e.Group("/api/v1")
	r.Use(middleware.JWT([]byte(helper.Getenv("JWT_SECRET"))))
	r.GET("/user/restricted", handler.Restricted)

	// Fetching data from api
	r.GET("/fetch/get-set-from-api", handler.GetAndInsertComidities)

	r.GET("/fetch/insert", handler.AddComidities)
	r.POST("/fetch/get-comudities", handler.GetAggregateDataComudities)
}
