package routers

import (
	"context"
	helper "efishery-api/helper"
	"efishery-api/model"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
	"github.com/labstack/echo"
)

var ctx = context.Background()

// GetAndInsertComidities is
func (h Handler) GetAndInsertComidities(c echo.Context) error {

	url := "https://stein.efishery.com/v1/storages/5e1edf521073e315924ceab4/list"

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	var result []model.Comudities

	json.Unmarshal([]byte(string(body)), &result)
	err = h.FetchUseCase.FetchDataComudities(result)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(200, result)
}

// AddComidities is
func (h Handler) AddComidities(c echo.Context) error {

	var param = new(model.Comudities)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	val, err := h.Redis.Get(ctx, "kursDollar").Result()
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	parseKurs, err := strconv.ParseFloat(val, 64)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	parsePrice, err := strconv.ParseFloat(param.Price, 64)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	param.UUID = uuid.New().String()
	param.Price = strconv.FormatFloat(parseKurs*parsePrice, 'E', -1, 64)
	err = h.FetchUseCase.AddComidities(*param)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(201, nil)
}

// GetAggregateDataComudities is
func (h Handler) GetAggregateDataComudities(c echo.Context) error {

	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	if strings.ToLower(claims["role"].(string)) != "admin" {
		return helper.Errors(c, 401, fmt.Errorf("User role "+claims["role"].(string)+" is not allowed"))
	}

	var param = new(model.GroupCommudities)
	if err := c.Bind(param); err != nil {
		return helper.Errors(c, 400, err)
	}

	group, err := h.FetchUseCase.GetAggregateDataComudities(*param)
	if err != nil {
		return helper.Errors(c, 400, err)
	}

	return c.JSON(200, group)
}
